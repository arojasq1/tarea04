package rojas.arturo.utils;

import rojas.arturo.bl.entities.material.Material;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.*;

public class LogManager {
    private static final Logger LOG_LOCAL = Logger.getLogger(LogManager.class.getName());
    public void registrarEntrada(String error){
        SimpleFormatter simpleFormatter = null;
        Handler fileHandler = null;
        // un ejemplo para un log simple
        LOG_LOCAL.info("Log paso 1 " + LOG_LOCAL.getName());
        //impresión de la fecha.
        LOG_LOCAL.config(LocalDate.now().toString());
        try {
            LocalDate fecha = LocalDate.of(2020,12,07);
            String fechaFormateada = fecha.format(DateTimeFormatter.ofPattern("dd-MM-yy"));
            fileHandler = new FileHandler("Log formateado" + fechaFormateada + ".log",true);
            simpleFormatter = new SimpleFormatter();
            LOG_LOCAL.addHandler(fileHandler);
            fileHandler.setFormatter(simpleFormatter);
            fileHandler.setLevel(Level.ALL);
            LOG_LOCAL.setLevel(Level.ALL);
            LOG_LOCAL.log(Level.FINE, "Inicio del programa");
        } catch (NullPointerException e) {
            LOG_LOCAL.warning("Error del sistema");
            LOG_LOCAL.log(Level.SEVERE, "Detalle de la excepción " + e.getMessage());
        } catch (IOException e) {
            LOG_LOCAL.log(Level.SEVERE,"Error en la aplicación " + e.getMessage());
        }

    }
}
