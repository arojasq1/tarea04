package rojas.arturo.utils;

import java.io.FileInputStream;
import java.util.Properties;

public class Utilities {
    public static String[] getProperties() throws Exception{
        String[] dbInfo = new String[2];
        Properties prop = new Properties();
        prop.load(new FileInputStream("db.props"));
        dbInfo[0] = prop.getProperty("driver");
        dbInfo[1] = prop.getProperty("url");
        return dbInfo;
    }

    public static String getMotorBD() throws Exception{
        Properties prop = new Properties();
        prop.load(new FileInputStream("db.props"));
        return prop.getProperty("motorBD");
    }
}
