package rojas.arturo.bl.logic;
import rojas.arturo.bl.entities.material.IMaterialDAO;
import rojas.arturo.bl.entities.material.Material;
import rojas.arturo.bl.entities.material.audiovisual.audio.Audio;
import rojas.arturo.bl.entities.material.audiovisual.audio.IAudioDAO;
import rojas.arturo.bl.entities.usuario.IUsuarioDAO;
import rojas.arturo.bl.entities.usuario.Usuario;
import rojas.arturo.bl.entities.usuario.UsuarioTextoDAO;
import rojas.arturo.dao.DAOFactory;
import rojas.arturo.utils.Utilities;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class Gestor {


    public String insertarMaterial(String id, LocalDate fechaCompra, String restringido, String tema) throws SQLException, Exception {
        try {
            Material m = new Material(id,fechaCompra,restringido,tema);
            DAOFactory factory = DAOFactory.getDaoFactory(Integer.parseInt(Utilities.getMotorBD()));
            IMaterialDAO dao = factory.getMaterialDAO();
            return dao.insertar(m);
        }
        catch (SQLException e){
            throw e;
        }
        catch (Exception e){
            throw e;
        }
    }

    public String[] getMateriales() throws Exception {
        DAOFactory factory = DAOFactory.getDaoFactory(Integer.parseInt(Utilities.getMotorBD()));
        IMaterialDAO dao = factory.getMaterialDAO();
        ArrayList<Material> materiales = dao.listar();
        String [] info = new String[materiales.size()];
        int pos = 0;
        for (Material m:materiales) {
            info[pos] = m.toString();
            pos++;
        }
        return info;
    }

    public String insertarUsuario(String id, String nombre, String apellido) throws SQLException, Exception {
        try {
            Usuario u = new Usuario(id,nombre,apellido);
            DAOFactory factory = DAOFactory.getDaoFactory(Integer.parseInt(Utilities.getMotorBD()));
            IUsuarioDAO dao = factory.getUsuarioDAO();
            return dao.insertar(u);
        }
        catch (SQLException e){
            throw e;
        }
        catch (Exception e){
            throw e;
        }
    }

    public String[] getUsuarios() throws Exception {
        DAOFactory factory = DAOFactory.getDaoFactory(Integer.parseInt(Utilities.getMotorBD()));
        IUsuarioDAO dao = factory.getUsuarioDAO();
        ArrayList<Usuario> usuarios = dao.listar();
        String [] info = new String[usuarios.size()];
        int pos = 0;
        for (Usuario u:usuarios) {
            info[pos] = u.toString();
            pos++;
        }
        return info;
    }
}
