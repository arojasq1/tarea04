package rojas.arturo.bl.entities.usuario;

import rojas.arturo.accesodatos.Conector;
import rojas.arturo.bl.entities.material.Material;
import rojas.arturo.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class UsuarioDAO implements IUsuarioDAO{
    @Override
    public String insertar(Usuario u) throws SQLException, Exception {
        try {
            String query = "INSERT INTO USUARIO(ID,NOMBRE,APELLIDO) VALUES('" +
                    u.getId() + "','" + u.getNombre() + "','" + u.getApellido() + "')";
            Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarQuery(query);
            System.out.println("Usuario registrado con éxito.");
            return "Usuario registrado con éxito";
        }
        catch (SQLException e){
            throw e;
        }
        catch (Exception e){
            throw e;
        }
    }

    @Override
    public ArrayList<Usuario> listar() throws SQLException, Exception {
        ArrayList<Usuario> usuarios = null;

        try {
            String query = "SELECT ID, NOMBRE, APELLIDO FROM USUARIO";
            ResultSet rs = Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (usuarios == null) {
                    usuarios = new ArrayList<>();
                }
                Usuario u = new Usuario(rs.getString("id"), rs.getString("nombre"),
                        rs.getString("apellido"));
                usuarios.add(u);
            }
            return usuarios;
        } catch (Exception e) {
            throw e;
        }
    }
}
