package rojas.arturo.bl.entities.usuario;

import rojas.arturo.bl.entities.usuario.administrativo.Administrativo;
import rojas.arturo.bl.entities.usuario.estudiante.Estudiante;
import rojas.arturo.bl.entities.usuario.profesor.Profesor;

import java.io.*;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class UsuarioTextoDAO implements IUsuarioDAO{
    private ArrayList<Usuario> usuarios;
    private static final String ListaUsuarios = "Usuarios.txt";
    private static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private static PrintStream out = System.out;

    @Override
    public String insertar(Usuario u) throws SQLException, Exception {
        try {
            String dato = u.toDatabase();
            FileWriter writer = new FileWriter(ListaUsuarios, true);
            BufferedWriter buffer = new BufferedWriter(writer);
            buffer.write(dato);
            buffer.newLine();
            buffer.close();
            out.println("Usuario guardado");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Insertar finalizado";
    }

    @Override
    public ArrayList<Usuario> listar() throws SQLException, Exception {
        boolean check=false;
        int tipo=0;
        do {
            out.println("\n1. Estudiante");
            out.println("2. Profesor");
            out.println("3. Administrativo");
            out.println("Escoga el tipo de usuario que desea listar.");
            tipo = Integer.parseInt(in.readLine());
            if(tipo==1||tipo==2||tipo==3){check=true;}else{out.println("Opción inválida.");
            }}while (!check);

        try {
            ArrayList<Usuario> usuarios = getUsuarios(tipo);
            if (usuarios != null) {
                for (Usuario u : usuarios) {
                    System.out.println(u.toString());
                }}else{System.out.println("No existe ningún registro de ese tipo.");}
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return usuarios;    }

    public void getUsuarioFromTxt(int ptipo) {
        try {
            FileReader reader = new FileReader(ListaUsuarios);
            BufferedReader buffer = new BufferedReader(reader);
            String datos;
            String[] infoUsuario;

            switch (ptipo) {
                case 1 -> {
                    while ((datos = buffer.readLine()) != null)
                    {
                        if (usuarios == null) {
                            usuarios = new ArrayList<>();
                        }
                        infoUsuario = datos.split(",");
                        Usuario u = new Usuario(infoUsuario[1], infoUsuario[2], infoUsuario[3]);
                        usuarios.add(u);
                    }
                    reader.close();
                }
                case 2 -> {
                    while ((datos = buffer.readLine()) != null)
                    {
                        if (usuarios == null) {
                            usuarios = new ArrayList<>();
                        }
                        infoUsuario = datos.split(",");
                        if (infoUsuario[0].equals("Estudiante")) {
                            Estudiante e = new Estudiante(infoUsuario[1], infoUsuario[2], infoUsuario[3], infoUsuario[4],
                                    Integer.parseInt(infoUsuario[5]));
                            usuarios.add(e);
                        }
                    }
                    reader.close();
                }
                case 3 -> {
                    while ((datos = buffer.readLine()) != null)
                    {
                        if (usuarios == null) {
                            usuarios = new ArrayList<>();
                        }
                        infoUsuario = datos.split(",");
                        if (infoUsuario[0].equals("Profesor")) {
                            Profesor p = new Profesor(infoUsuario[1], infoUsuario[2], infoUsuario[3], infoUsuario[4],
                                    LocalDate.parse(infoUsuario[5]));
                            usuarios.add(p);
                        }
                    }
                    reader.close();
                }
                case 4 -> {
                    while ((datos = buffer.readLine()) != null)
                    {
                        if (usuarios == null) {
                            usuarios = new ArrayList<>();
                        }
                        infoUsuario = datos.split(",");
                        if (infoUsuario[0].equals("Administrativo")) {
                            Administrativo a = new Administrativo(infoUsuario[1], infoUsuario[2], infoUsuario[3],
                                    infoUsuario[4], Double.parseDouble(infoUsuario[5]));
                            usuarios.add(a);
                        }
                    }
                    reader.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Usuario> getUsuarios(int ptipo) {
        getUsuarioFromTxt(ptipo);
        return usuarios;
    }
}
