package rojas.arturo.bl.entities.usuario;

public class Usuario {
    protected String id;
    protected String nombre;
    protected String apellido;

    public Usuario() {
    }

    public Usuario(String id, String nombre, String apellido) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String toDatabase() {
        return this.getClass().getSimpleName() + "," +  this.getId() + "," + this.getNombre() + "," + this.getApellido();
    }

    @Override
    public boolean equals(Object o){
        boolean duplicate = false;

        if (o instanceof Usuario) {
            Usuario u = (Usuario) o;
            duplicate = u.id.equals(this.id);
        }
        return duplicate;
    }


    @Override
    public String toString() {
        return "Usuario{" +
                "id='" + id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                '}';
    }
}
