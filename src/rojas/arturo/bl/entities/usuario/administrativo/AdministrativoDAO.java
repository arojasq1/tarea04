package rojas.arturo.bl.entities.usuario.administrativo;

import rojas.arturo.accesodatos.Conector;
import rojas.arturo.bl.entities.usuario.Usuario;
import rojas.arturo.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AdministrativoDAO implements IAdministrativoDAO{
    @Override
    public String insertar(Administrativo a) throws SQLException, Exception {
        try {
            String query = "INSERT INTO ADMINISTRATIVO(ID,NOMBRE,APELLIDO,NOMBRAMIENTO,HORAS) VALUES('" +
                    a.getId() + "','" + a.getNombre() + "','" + a.getApellido() + "','" + a.getNombramiento() + "','" + a.getHoras() + "')";
            Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarQuery(query);
            System.out.println("Administrativo registrado con éxito.");
            return "Administrativo registrado con éxito";
        }
        catch (SQLException e){
            throw e;
        }
        catch (Exception e){
            throw e;
        }
    }

    @Override
    public ArrayList<Administrativo> listar() throws SQLException, Exception {
        ArrayList<Administrativo> administrativos = null;

        try {
            String query = "SELECT ID, NOMBRE, APELLIDO,NOMBRAMIENTO,HORAS FROM ADMINISTRATIVO";
            ResultSet rs = Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (administrativos == null) {
                    administrativos = new ArrayList<>();
                }
                Administrativo a = new Administrativo(rs.getString("id"), rs.getString("nombre"),
                        rs.getString("apellido"),rs.getString("nombramientos"),Integer.parseInt(rs.getString("horas")));
                administrativos.add(a);
            }
            return administrativos;
        } catch (Exception e) {
            throw e;
        }
    }
}
