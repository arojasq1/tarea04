package rojas.arturo.bl.entities.usuario.administrativo;

import rojas.arturo.bl.entities.usuario.Usuario;

public class Administrativo extends Usuario {
    private String nombramiento;
    private double horas;

    public Administrativo() {
        super();
    }

    public Administrativo(String id, String nombre, String apellido, String nombramiento, double horas) {
        super(id, nombre, apellido);
        this.nombramiento = nombramiento;
        this.horas = horas;
    }

    public String getNombramiento() {
        return nombramiento;
    }

    public void setNombramiento(String nombramiento) {
        this.nombramiento = nombramiento;
    }

    public double getHoras() {
        return horas;
    }

    public void setHoras(double horas) {
        this.horas = horas;
    }

    public String toDatabase() {
        return this.getClass().getSimpleName() + "," + this.getId() + "," + this.getNombre() + "," + this.getApellido() + ","
                + this.getNombramiento() + "," + this.getHoras();
    }

    @Override
    public String toString() {
        return "Administrativo{" +
                "id=" + id +
                ", nombre=" + nombre +
                ", apellido='" + apellido + '\'' +
                ", nombramiento='" + nombramiento + '\'' +
                ", horas semanales='" + horas + '\'' +
                '}';
    }

}
