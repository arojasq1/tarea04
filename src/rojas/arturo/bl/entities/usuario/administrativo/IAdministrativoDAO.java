package rojas.arturo.bl.entities.usuario.administrativo;

import java.sql.*;
import java.util.ArrayList;

public interface IAdministrativoDAO {
    String insertar(Administrativo a) throws SQLException,Exception;
    ArrayList<Administrativo> listar() throws SQLException,Exception;
}
