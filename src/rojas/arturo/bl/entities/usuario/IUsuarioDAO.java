package rojas.arturo.bl.entities.usuario;

import java.sql.*;
import java.util.ArrayList;

public interface IUsuarioDAO {
    String insertar(Usuario u) throws SQLException,Exception;
    ArrayList<Usuario> listar() throws SQLException,Exception;
}
