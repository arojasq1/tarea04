package rojas.arturo.bl.entities.usuario.estudiante;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IEstudianteDAO {
    String insertar(Estudiante e) throws SQLException,Exception;
    ArrayList<Estudiante> listar() throws SQLException,Exception;
}
