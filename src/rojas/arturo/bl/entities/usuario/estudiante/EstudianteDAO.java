package rojas.arturo.bl.entities.usuario.estudiante;

import rojas.arturo.accesodatos.Conector;
import rojas.arturo.bl.entities.usuario.administrativo.Administrativo;
import rojas.arturo.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class EstudianteDAO implements IEstudianteDAO{
    @Override
    public String insertar(Estudiante e) throws SQLException, Exception {
        try {
            String query = "INSERT INTO ESTUDIANTE(ID,NOMBRE,APELLIDO,CARRERA,CREDITOSCUATRI) VALUES('" +
                    e.getId() + "','" + e.getNombre() + "','" + e.getApellido() + "','" + e.getCarrera() + "','" + e.getCreditosCuatri() + "')";
            Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarQuery(query);
            System.out.println("Estudiante registrado con éxito.");
            return "Estudiante registrado con éxito";
        }
        catch (SQLException ex){
            throw ex;
        }
        catch (Exception ex){
            throw ex;
        }
    }

    @Override
    public ArrayList<Estudiante> listar() throws SQLException, Exception {
        ArrayList<Estudiante> estudiantes = null;

        try {
            String query = "SELECT ID, NOMBRE, APELLIDO, CARRERA, CREDITOSCUATRI FROM ESTUDIANTE";
            ResultSet rs = Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (estudiantes == null) {
                    estudiantes = new ArrayList<>();
                }
                Estudiante e = new Estudiante(rs.getString("id"), rs.getString("nombre"),
                        rs.getString("apellido"),rs.getString("carrera"),Integer.parseInt(rs.getString("creditoscuatri")));
                estudiantes.add(e);
            }
            return estudiantes;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
