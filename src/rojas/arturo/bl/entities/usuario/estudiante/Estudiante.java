package rojas.arturo.bl.entities.usuario.estudiante;

import rojas.arturo.bl.entities.usuario.Usuario;

public class Estudiante extends Usuario {
    private String carrera;
    private int creditosCuatri;

    public Estudiante() {
        super();
    }

    public Estudiante(String id, String nombre, String apellido, String carrera, int creditosCuatri) {
        super(id, nombre, apellido);
        this.carrera = carrera;
        this.creditosCuatri = creditosCuatri;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public int getCreditosCuatri() {
        return creditosCuatri;
    }

    public void setCreditosCuatri(int creditosCuatri) {
        this.creditosCuatri = creditosCuatri;
    }

    public String toDatabase() {
        return this.getClass().getSimpleName() + "," + this.getId() + ","
                + this.getNombre() + "," + this.getApellido() + "," + this.getCarrera() + "," + this.getCreditosCuatri();
    }

    @Override
    public String toString() {
        return "Estudiante{" +
                "id='" + id + '\'' +
                ", nombre=" + nombre +
                ", apellido='" + apellido + '\'' +
                ", carrera='" + carrera + '\'' +
                ", creditos='" + creditosCuatri + '\'' +
                '}';
    }
}
