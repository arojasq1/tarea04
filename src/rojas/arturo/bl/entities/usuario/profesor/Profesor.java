package rojas.arturo.bl.entities.usuario.profesor;

import rojas.arturo.bl.entities.usuario.Usuario;

import java.time.LocalDate;

public class Profesor extends Usuario {
    private String contrato;
    private LocalDate contratacion;

    public Profesor() {
        super();
    }

    public Profesor(String id, String nombre, String apellido, String contrato, LocalDate contratacion) {
        super(id, nombre, apellido);
        this.contrato = contrato;
        this.contratacion = contratacion;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public LocalDate getContratacion() {
        return contratacion;
    }

    public void setContratacion(LocalDate contratacion) {
        this.contratacion = contratacion;
    }

    public String toDatabase() {
        return this.getClass().getSimpleName() + "," + this.getId() + "," + this.getNombre() + "," +
                this.getApellido() + "," +  this.getContrato() + "," + this.getContratacion();
    }

    @Override
    public String toString() {
        return "Profesor{" +
                "id=" + id +
                ", nombre=" + nombre +
                ", apellido='" + apellido + '\'' +
                ", contrato='" + contrato + '\'' +
                ", contratacion='" + contratacion + '\'' +
                '}';
    }
}
