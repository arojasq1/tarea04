package rojas.arturo.bl.entities.usuario.profesor;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IProfesorDao {
    String insertar(Profesor p) throws SQLException,Exception;
    ArrayList<Profesor> listar() throws SQLException,Exception;
}
