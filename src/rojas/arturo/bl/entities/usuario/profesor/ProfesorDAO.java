package rojas.arturo.bl.entities.usuario.profesor;

import rojas.arturo.accesodatos.Conector;
import rojas.arturo.bl.entities.usuario.estudiante.Estudiante;
import rojas.arturo.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ProfesorDAO implements IProfesorDao{
    @Override
    public String insertar(Profesor p) throws SQLException, Exception {
        try {
            String query = "INSERT INTO PROFESOR(ID,NOMBRE,APELLIDO,CONTRATO,CONTRATACION) VALUES('" +
                    p.getId() + "','" + p.getNombre() + "','" + p.getApellido() + "','" + p.getContrato() + "','" + p.getContratacion() + "')";
            Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarQuery(query);
            System.out.println("Profesor registrado con éxito.");
            return "Profesor registrado con éxito";
        }
        catch (SQLException ex){
            throw ex;
        }
        catch (Exception ex){
            throw ex;
        }
    }

    @Override
    public ArrayList<Profesor> listar() throws SQLException, Exception {
        ArrayList<Profesor> profesores = null;

        try {
            String query = "SELECT ID, NOMBRE, APELLIDO, CONTRATO, CONTRATACION FROM PROFESOR";
            ResultSet rs = Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (profesores == null) {
                    profesores = new ArrayList<>();
                }
                Profesor p = new Profesor(rs.getString("id"), rs.getString("nombre"),
                        rs.getString("apellido"),rs.getString("contrato"), LocalDate.parse(rs.getString("contratacion")));
                profesores.add(p);
            }
            return profesores;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
