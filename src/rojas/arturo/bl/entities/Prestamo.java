package rojas.arturo.bl.entities;

import rojas.arturo.bl.entities.material.Material;
import rojas.arturo.bl.entities.usuario.Usuario;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class Prestamo {

    private Usuario usuario;
    private ArrayList<Material> materiales;
    private LocalDate devolucion;
    private int multa;

    public Prestamo() {
    }

    public Prestamo(Usuario usuario, ArrayList<Material> materiales, LocalDate devolucion) {
        this.usuario = usuario;
        this.materiales = materiales;
        this.devolucion = devolucion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public ArrayList<Material> getMateriales() {
        return materiales;
    }

    public void setMateriales(ArrayList<Material> materiales) {
        this.materiales = materiales;
    }

    public LocalDate getDevolucion() {
        return devolucion;
    }

    public void setDevolucion(LocalDate devolucion) {
        this.devolucion = devolucion;
    }

    public int getMulta() {
        calcularMulta();
        return multa;
    }

    public void setMulta(int multa) {
        this.multa = multa;
    }

    @Override
    public String toString() {
        return "Prestamo{" +
                "usuario=" + usuario +
                ", materiales=" + materiales +
                ", devolucion=" + devolucion +
                ", multa=" + calcularMulta() + " colones." +
                '}';
    }

    private int calcularMulta() {
        LocalDate hoy = LocalDate.now();
        int multa;

        long result = ChronoUnit.DAYS.between(hoy,devolucion);
        if (result>0) {
            result = 0;
        }

        multa = Math.abs((int) (150*result));
        this.setMulta(multa);
        return multa;
    }



}
