package rojas.arturo.bl.entities.material;

import rojas.arturo.utils.Utilities;
import rojas.arturo.accesodatos.Conector;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;


public class MaterialDAO implements IMaterialDAO{
    @Override
    public String insertar(Material m) throws SQLException, Exception {
        try {
            String query = "INSERT INTO MATERIAL(ID,FECHA_COMPRA,RESTRINGIDO,TEMA) VALUES('" +
                    m.getId() + "','" + m.getFechaCompra() + "','" + m.getRestringido() + "','" +
                    m.getTema() + "')";
            Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarQuery(query);
            System.out.println("Material registrado con éxito.");
            return "Material registrado con éxito";
        }
        catch (SQLException e){
            throw e;
        }
        catch (Exception e){
            throw e;
        }
    }

    @Override
    public ArrayList<Material> listar() throws SQLException, Exception {
        ArrayList<Material> materiales = null;

        try {
            String query = "SELECT ID, FECHA_COMPRA, RESTRINGIDO, TEMA FROM MATERIAL";
            ResultSet rs = Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (materiales == null) {
                    materiales = new ArrayList<>();
                }
                Material m = new Material(rs.getString("id"), LocalDate.parse(rs.getString("fecha_compra")),
                        rs.getString("restringido"),rs.getString("tema"));
                materiales.add(m);
            }
            return materiales;
        } catch (Exception e) {
            throw e;
        }
    }
}
