package rojas.arturo.bl.entities.material.audiovisual.texto;

import java.sql.*;
import java.util.ArrayList;

public interface ITextoDAO {
    String insertar(Texto t) throws SQLException,Exception;
    ArrayList<Texto> listar() throws SQLException,Exception;
}
