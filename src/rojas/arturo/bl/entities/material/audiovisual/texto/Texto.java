package rojas.arturo.bl.entities.material.audiovisual.texto;

import rojas.arturo.bl.entities.material.Material;

import java.time.LocalDate;

public class Texto extends Material {

    private String titulo;
    private String autor;
    private LocalDate publicacion;
    private int numPaginas;
    private String idioma;

    public Texto() {
        super();
    }

    public Texto(String id, LocalDate fechaCompra, String restringido, String tema, String titulo, String autor, LocalDate publicacion, int numPaginas, String idioma) {
        super(id, fechaCompra, restringido, tema);
        this.titulo = titulo;
        this.autor = autor;
        this.publicacion = publicacion;
        this.numPaginas = numPaginas;
        this.idioma = idioma;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public LocalDate getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(LocalDate publicacion) {
        this.publicacion = publicacion;
    }

    public int getNumPaginas() {
        return numPaginas;
    }

    public void setNumPaginas(int numPaginas) {
        this.numPaginas = numPaginas;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String toDatabase(){
        return this.getClass().getSimpleName()+","+this.getId()+","+this.getFechaCompra()+","
                +this.getRestringido()+","+this.getTema()+","+this.getTitulo()+","+this.getAutor()
                +","+this.getPublicacion()+","+this.getNumPaginas()+","+this.getIdioma();
    }

    @Override
    public String toString() {
        return "Texto{" +
                "titulo='" + titulo + '\'' +
                ", autor='" + autor + '\'' +
                ", publicacion=" + publicacion +
                ", numPaginas=" + numPaginas +
                ", idioma='" + idioma + '\'' +
                ", id='" + id + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido='" + restringido + '\'' +
                ", tema='" + tema + '\'' +
                '}';
    }
}
