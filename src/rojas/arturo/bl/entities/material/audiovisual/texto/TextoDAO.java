package rojas.arturo.bl.entities.material.audiovisual.texto;

import rojas.arturo.accesodatos.Conector;
import rojas.arturo.bl.entities.material.audiovisual.video.Video;
import rojas.arturo.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class TextoDAO implements ITextoDAO{
    @Override
    public String insertar(Texto t) throws SQLException, Exception {
        try {
            String query = "INSERT INTO TEXTO(ID,FECHA_COMPRA,RESTRINGIDO,TEMA,TITULO,AUTOR,PUBLICACION,NUM_PAGINAS,IDIOMA) VALUES('" +
                    t.getId() + "','" + t.getFechaCompra() + "','" + t.getRestringido() + "','" +
                    t.getTema() + "','" + t.getTitulo() + "','" + t.getAutor() + "','" + t.getPublicacion() + "','" + t.getNumPaginas() + "','" + t.getIdioma() + "')";
            Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
            System.out.println("Texto registrado con éxito.");
            return "Texto registrado con éxito";
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public ArrayList<Texto> listar() throws SQLException, Exception {
        ArrayList<Texto> textos = null;

        try {
            String query = "SELECT ID, FECHA_COMPRA, RESTRINGIDO, TEMA, TITULO, AUTOR, PUBLICACION, NUM_PAGINAS, IDIOMA FROM TEXTO";
            ResultSet rs = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (textos == null) {
                    textos = new ArrayList<>();
                }
                Texto t = new Texto(rs.getString("id"), LocalDate.parse(rs.getString("fecha_compra")),
                        rs.getString("restringido"), rs.getString("tema"), rs.getString("titulo"),
                        rs.getString("autor"), LocalDate.parse(rs.getString("publicacion")), Integer.parseInt(rs.getString("num_paginas")),rs.getString("idioma"));
                textos.add(t);
            }
            return textos;
        } catch (Exception e) {
            throw e;
        }
    }
}
