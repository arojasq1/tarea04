package rojas.arturo.bl.entities.material.audiovisual;

import java.sql.*;
import java.util.ArrayList;

public interface IAudiovisualDAO {
    String insertar(Audiovisual a) throws SQLException,Exception;
    ArrayList<Audiovisual> listar() throws SQLException,Exception;
}
