package rojas.arturo.bl.entities.material.audiovisual;

import rojas.arturo.bl.entities.material.Material;

import java.time.LocalDate;

public class Audiovisual extends Material {
    protected String formato;
    protected int duracion;
    protected String idioma;

    public Audiovisual() {
        super();
    }

    public Audiovisual(String id, LocalDate fechaCompra, String restringido, String tema, String formato, int duracion, String idioma) {
        super(id, fechaCompra, restringido, tema);
        this.formato = formato;
        this.duracion = duracion;
        this.idioma = idioma;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String toDatabase() {
        return this.getClass().getSimpleName() + "," + this.getId() + "," + this.getFechaCompra() + "," + this.getRestringido() + "," + this.getTema() + "," + this.getFormato()
                + "," + this.getDuracion() + "," + this.getIdioma();
    }

    @Override
    public String toString() {
        return "Audiovisual{" +
                "formato='" + formato + '\'' +
                ", duracion=" + duracion +
                ", idioma='" + idioma + '\'' +
                ", id='" + id + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido=" + restringido +
                ", tema='" + tema + '\'' +
                '}';
    }
}
