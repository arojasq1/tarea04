package rojas.arturo.bl.entities.material.audiovisual.audio;

import rojas.arturo.bl.entities.material.audiovisual.Audiovisual;

import java.time.LocalDate;

public class Audio extends Audiovisual {

    public Audio() {
        super();
    }

    public Audio(String id, LocalDate fechaCompra, String restringido, String tema, String formato, int duracion, String idioma) {
        super(id, fechaCompra, restringido, tema, formato, duracion, idioma);
    }

    public String toDatabase(){
        return this.getClass().getSimpleName()+","+this.getId()+","+this.getFechaCompra()+"," +
                ""+this.getRestringido()+","+this.getTema()+","+this.getFormato()+","+this.getDuracion()
                +","+this.getIdioma();
    }

    @Override
    public String toString() {
        return "Audio{" +
                "formato='" + formato + '\'' +
                ", duracion=" + duracion +
                ", idioma='" + idioma + '\'' +
                ", id='" + id + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido=" + restringido +
                ", tema='" + tema + '\'' +
                '}';
    }



}
