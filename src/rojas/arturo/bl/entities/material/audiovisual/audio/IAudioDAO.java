package rojas.arturo.bl.entities.material.audiovisual.audio;

import java.sql.*;
import java.util.ArrayList;

public interface IAudioDAO {
    String insertar(Audio a) throws SQLException,Exception;
    ArrayList<Audio> listar() throws SQLException,Exception;
}
