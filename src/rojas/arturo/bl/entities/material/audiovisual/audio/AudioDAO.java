package rojas.arturo.bl.entities.material.audiovisual.audio;

import rojas.arturo.utils.Utilities;
import rojas.arturo.accesodatos.Conector;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class AudioDAO implements IAudioDAO{
    @Override
    public String insertar(Audio a) throws SQLException, Exception {
        try {
            String query = "INSERT INTO AUDIO(ID,FECHA_COMPRA,RESTRINGIDO,TEMA,FORMATO,DURACION,IDIOMA) VALUES('" +
                    a.getId() + "','" + a.getFechaCompra() + "','" + a.getRestringido() + "','" +
                    a.getTema() + "','" + a.getFormato() + "','" + a.getDuracion() + "','" + a.getIdioma() + "')";
            Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarQuery(query);
            System.out.println("Audio registrado con éxito.");
            return "Audio registrado con éxito";
        }catch (SQLException e){
            throw e;
        }catch (Exception e){
            throw e;
        }
    }

    @Override
    public ArrayList<Audio> listar() throws SQLException, Exception {
        ArrayList<Audio> audios = null;

        try {
            String query = "SELECT ID, FECHA_COMPRA, RESTRINGIDO, TEMA, FORMATO, DURACION, IDIOMA FROM AUDIO";
            ResultSet rs = Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (audios == null) {
                    audios = new ArrayList<>();
                }
                Audio a = new Audio(rs.getString("id"), LocalDate.parse(rs.getString("fecha_compra")),
                        rs.getString("restringido"),rs.getString("tema"),rs.getString("formato"),
                        Integer.parseInt(rs.getString("duracion")),rs.getString("idioma"));
                audios.add(a);
            }
            return audios;
        } catch (Exception e) {
            throw e;
        }
    }
}