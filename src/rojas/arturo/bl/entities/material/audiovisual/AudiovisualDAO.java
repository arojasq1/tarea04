package rojas.arturo.bl.entities.material.audiovisual;

import rojas.arturo.accesodatos.Conector;
import rojas.arturo.utils.Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class AudiovisualDAO implements IAudiovisualDAO{
    @Override
    public String insertar(Audiovisual a) throws SQLException, Exception {
        try {
            String query = "INSERT INTO AUDIOVISUAL(ID,FECHA_COMPRA,RESTRINGIDO,TEMA,FORMATO,DURACION,IDIOMA) VALUES('" +
                    a.getId() + "','" + a.getFechaCompra() + "','" + a.getRestringido() + "','" +
                    a.getTema() + "','" + a.getFormato() + "','" + a.getDuracion() + "','" + a.getIdioma() + "')";
            Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarQuery(query);
            System.out.println("Audiovisual registrado con éxito.");
            return "Audiovisual registrado con éxito";
        }
        catch (SQLException e){
            throw e;
        }
        catch (Exception e){
            throw e;
        }
    }

    @Override
    public ArrayList<Audiovisual> listar() throws SQLException, Exception {
        ArrayList<Audiovisual> audiovisuales = null;

        try {
            String query = "SELECT ID, FECHA_COMPRA, RESTRINGIDO, TEMA, FORMATO, DURACION, IDIOMA FROM AUDIOVISUAL";
            ResultSet rs = Conector.getConector(Utilities.getProperties()[0],Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (audiovisuales == null) {
                    audiovisuales = new ArrayList<>();
                }
                Audiovisual a = new Audiovisual(rs.getString("id"), LocalDate.parse(rs.getString("fecha_compra")),
                        rs.getString("restringido"),rs.getString("tema"),rs.getString("formato"),
                        Integer.parseInt(rs.getString("duracion")),rs.getString("idioma"));
                audiovisuales.add(a);
            }
            return audiovisuales;
        } catch (Exception e) {
            throw e;
        }
    }
}
