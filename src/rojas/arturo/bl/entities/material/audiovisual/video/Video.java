package rojas.arturo.bl.entities.material.audiovisual.video;

import rojas.arturo.bl.entities.material.audiovisual.Audiovisual;

import java.time.LocalDate;

public class Video extends Audiovisual {

    private String director;

    public Video() {
        super();
    }

    public Video(String id, LocalDate fechaCompra, String restringido, String tema, String formato, int duracion, String idioma, String director) {
        super(id, fechaCompra, restringido, tema, formato, duracion, idioma);
        this.director = director;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String toDatabase(){
        return this.getClass().getSimpleName()+","+this.getId()+","+this.getFechaCompra()+","
                +this.getRestringido()+","+this.getTema()+","+this.getFormato()+","+this.getDuracion()
                +","+this.getIdioma()+","+this.getDirector();
    }

    @Override
    public String toString() {
        return "Video{" +
                "director='" + director + '\'' +
                ", formato='" + formato + '\'' +
                ", duracion=" + duracion +
                ", idioma='" + idioma + '\'' +
                ", id='" + id + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido=" + restringido +
                ", tema='" + tema + '\'' +
                '}';
    }
}
