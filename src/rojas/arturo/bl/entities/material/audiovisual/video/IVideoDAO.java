package rojas.arturo.bl.entities.material.audiovisual.video;

import rojas.arturo.bl.entities.material.audiovisual.audio.Audio;

import java.sql.*;
import java.util.ArrayList;

public interface IVideoDAO {
    String insertar(Video v) throws SQLException,Exception;
    ArrayList<Video> listar() throws SQLException,Exception;
}
