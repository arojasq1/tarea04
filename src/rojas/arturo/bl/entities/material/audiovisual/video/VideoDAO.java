package rojas.arturo.bl.entities.material.audiovisual.video;

import rojas.arturo.utils.Utilities;
import rojas.arturo.accesodatos.Conector;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class VideoDAO implements IVideoDAO {
    @Override
    public String insertar(Video v) throws SQLException, Exception {
        try {
            String query = "INSERT INTO VIDEO(ID,FECHA_COMPRA,RESTRINGIDO,TEMA,FORMATO,DURACION,IDIOMA,DIRECTOR) VALUES('" +
                    v.getId() + "','" + v.getFechaCompra() + "','" + v.getRestringido() + "','" +
                    v.getTema() + "','" + v.getFormato() + "','" + v.getDuracion() + "','" + v.getIdioma() + "','" + v.getDirector() + "')";
            Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
            System.out.println("Video registrado con éxito.");
            return "Video registrado con éxito";
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public ArrayList<Video> listar() throws SQLException, Exception {
        ArrayList<Video> videos = null;

        try {
            String query = "SELECT ID, FECHA_COMPRA, RESTRINGIDO, TEMA, FORMATO, DURACION, IDIOMA, DIRECTOR FROM VIDEO";
            ResultSet rs = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (videos == null) {
                    videos = new ArrayList<>();
                }
                Video v = new Video(rs.getString("id"), LocalDate.parse(rs.getString("fecha_compra")),
                        rs.getString("restringido"), rs.getString("tema"), rs.getString("formato"),
                        Integer.parseInt(rs.getString("duracion")), rs.getString("idioma"), rs.getString("director"));
                videos.add(v);
            }
            return videos;
        } catch (Exception e) {
            throw e;
        }
    }
}