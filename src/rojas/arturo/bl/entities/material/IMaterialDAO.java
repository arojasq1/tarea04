package rojas.arturo.bl.entities.material;

import java.sql.*;
import java.util.ArrayList;

public interface IMaterialDAO {
    String insertar(Material m) throws SQLException,Exception;
    ArrayList<Material> listar() throws SQLException,Exception;
}
