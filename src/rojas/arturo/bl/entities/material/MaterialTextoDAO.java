package rojas.arturo.bl.entities.material;

import java.util.ArrayList;

import rojas.arturo.bl.entities.material.audiovisual.audio.Audio;
import rojas.arturo.bl.entities.material.audiovisual.texto.Texto;
import rojas.arturo.bl.entities.material.audiovisual.video.Video;
import rojas.arturo.bl.entities.material.otro.Otro;

import java.io.*;
import java.time.LocalDate;


public class MaterialTextoDAO implements IMaterialDAO{
    private ArrayList<Material> materiales;
    private static final String ListaMateriales = "Materiales.txt";
    private static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private static PrintStream out = System.out;

    @Override
    public String insertar(Material m) throws Exception {
        try {
            String dato = m.toDatabase();
            FileWriter writer = new FileWriter(ListaMateriales, true);
            BufferedWriter buffer = new BufferedWriter(writer);
            buffer.write(dato);
            buffer.newLine();
            buffer.close();
            out.println("Material guardado");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Insertar finalizado";
    }

    @Override
    public ArrayList<Material> listar() throws Exception {
        boolean check=false;
        int tipo=0;
        do {
            out.println("\n1. Audio");
            out.println("2. Video");
            out.println("3. Texto");
            out.println("4. Otro");
            out.println("Escoga el tipo de material que desea listar.");
            tipo = Integer.parseInt(in.readLine());
            if(tipo==1||tipo==2||tipo==3||tipo==4){check=true;}else{out.println("Opción inválida.");
            }}while (!check);

        try {
            ArrayList<Material> materiales = getMateriales(tipo);
            if (materiales != null) {
                for (Material m : materiales) {
                    System.out.println(m.toString());
                }}else{System.out.println("No existe ningún registro de ese tipo.");}
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return materiales;
    }

    public void getMatFromTXT(int ptipo) throws Exception {
        try {
            FileReader reader = new FileReader(ListaMateriales);
            BufferedReader buffer = new BufferedReader(reader);
            String datos;
            String[] infoMaterial;

            switch (ptipo) {
                case 1 -> {
                    while ((datos = buffer.readLine()) != null)
                    {
                        if (materiales == null) {
                            materiales = new ArrayList<>();
                        }
                        infoMaterial = datos.split(",");
                        if (infoMaterial[0].equals("Audio")) {
                            Audio a = new Audio(infoMaterial[1],LocalDate.parse(infoMaterial[2]),infoMaterial[3],infoMaterial[4],infoMaterial[5],Integer.parseInt(infoMaterial[6]),
                                    infoMaterial[7]);
                            materiales.add(a);
                        }
                    }
                    reader.close();
                }
                case 2 -> {
                    while ((datos = buffer.readLine()) != null)
                    {
                        if (materiales == null) {
                            materiales = new ArrayList<>();
                        }
                        infoMaterial = datos.split(",");
                        if (infoMaterial[0].equals("Video")) {
                            Video v = new Video(infoMaterial[1],LocalDate.parse(infoMaterial[2]),infoMaterial[3],infoMaterial[4],infoMaterial[5],Integer.parseInt(infoMaterial[6]),
                                    infoMaterial[7],infoMaterial[8]);
                            materiales.add(v);
                        }
                    }
                    reader.close();
                }
                case 3 -> {
                    while ((datos = buffer.readLine()) != null)
                    {
                        if (materiales == null) {
                            materiales = new ArrayList<>();
                        }
                        infoMaterial = datos.split(",");
                        if (infoMaterial[0].equals("Texto")) {
                            Texto t = new Texto(infoMaterial[1],LocalDate.parse(infoMaterial[2]),infoMaterial[3],infoMaterial[4],infoMaterial[5],infoMaterial[6],
                                    LocalDate.parse(infoMaterial[7]),Integer.parseInt(infoMaterial[8]),infoMaterial[9]);
                            materiales.add(t);
                        }
                    }
                    reader.close();
                }
                case 4 -> {
                    while ((datos = buffer.readLine()) != null)
                    {
                        if (materiales == null) {
                            materiales = new ArrayList<>();
                        }
                        infoMaterial = datos.split(",");
                        if (infoMaterial[0].equals("Otro")) {
                            Otro o = new Otro(infoMaterial[1],LocalDate.parse(infoMaterial[2]),infoMaterial[3],infoMaterial[4],infoMaterial[5]);
                            materiales.add(o);
                        }
                    }
                    reader.close();
                }
                case 5 -> {
                    while ((datos = buffer.readLine()) != null)
                    {
                        if (materiales == null) {
                            materiales = new ArrayList<>();
                        }
                        infoMaterial = datos.split(",");
                        Material m = new Material(infoMaterial[1],LocalDate.parse(infoMaterial[2]),infoMaterial[3],infoMaterial[4]);
                        materiales.add(m);
                    }
                    reader.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void registrar(Material m) throws Exception {
        insertar(m);
    }

    public ArrayList<Material> getMateriales(int ptipo) throws Exception {
        getMatFromTXT(ptipo);
        return materiales;
    }
}
