package rojas.arturo.bl.entities.material.otro;

import java.sql.*;
import java.util.ArrayList;

public interface IOtroDAO {
    String insertar(Otro o) throws SQLException,Exception;
    ArrayList<Otro> listar() throws SQLException,Exception;
}
