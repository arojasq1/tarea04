package rojas.arturo.bl.entities.material.otro;

import rojas.arturo.bl.entities.material.Material;

import java.time.LocalDate;

public class Otro extends Material {
    private String descripcion;

    public Otro() {
        super();
    }

    public Otro(String id, LocalDate fechaCompra, String restringido, String tema, String descripcion) {
        super(id, fechaCompra, restringido, tema);
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String toDatabase(){
        return this.getClass().getSimpleName()+","+this.getId()+","+this.getFechaCompra()+","+this.getRestringido()+
                ","+this.getTema()+","+this.getDescripcion();
    }

    @Override
    public String toString() {
        return "Otro{" +
                "descripcion='" + descripcion + '\'' +
                ", id='" + id + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido='" + restringido + '\'' +
                ", tema='" + tema + '\'' +
                '}';
    }
}
