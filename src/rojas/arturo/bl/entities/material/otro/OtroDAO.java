package rojas.arturo.bl.entities.material.otro;

import rojas.arturo.utils.Utilities;
import rojas.arturo.accesodatos.Conector;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class OtroDAO implements IOtroDAO{
    @Override
    public String insertar(Otro o) throws SQLException, Exception {
        try {
            String query = "INSERT INTO OTRO(ID,FECHA_COMPRA,RESTRINGIDO,TEMA,DESCRIPCION) VALUES('" +
                    o.getId() + "','" + o.getFechaCompra() + "','" + o.getRestringido() + "','" +
                    o.getTema() + "','" + o.getDescripcion() + "')";
            Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarQuery(query);
            System.out.println("Otro registrado con éxito.");
            return "Otro registrado con éxito";
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public ArrayList<Otro> listar() throws SQLException, Exception {
        ArrayList<Otro> otros = null;

        try {
            String query = "SELECT ID, FECHA_COMPRA, RESTRINGIDO, TEMA, FORMATO, DESCRIPCION FROM OTRO";
            ResultSet rs = Conector.getConector(Utilities.getProperties()[0], Utilities.getProperties()[1]).ejecutarSQL(query);
            while (rs.next()) {
                if (otros == null) {
                    otros = new ArrayList<>();
                }
                Otro o = new Otro(rs.getString("id"), LocalDate.parse(rs.getString("fecha_compra")),
                        rs.getString("restringido"), rs.getString("tema"), rs.getString("descripcion"));
                otros.add(o);
            }
            return otros;
        } catch (Exception e) {
            throw e;
        }
    }
}
