package rojas.arturo.bl.entities.material;

import rojas.arturo.bl.entities.usuario.Usuario;

import java.time.LocalDate;

public class Material {
    protected String id;
    protected LocalDate fechaCompra;
    protected String restringido;
    protected String tema;

    public Material() {
    }

    public Material(String id, LocalDate fechaCompra, String restringido, String tema) {
        this.id = id;
        this.fechaCompra = fechaCompra;
        this.restringido = restringido;
        this.tema = tema;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public String getRestringido() {
        return restringido;
    }

    public void setRestringido(String restringido) {
        this.restringido = restringido;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String toDatabase() {
        return this.getClass().getSimpleName() + "," + this.getId() + "," + this.getFechaCompra() + "," + this.getRestringido() + "," + this.getTema();
    }

    @Override
    public boolean equals(Object o){
        boolean duplicate = false;

        if (o instanceof Material) {
            Material u = (Material) o;
            duplicate = u.id.equals(this.id);
        }
        return duplicate;
    }

    @Override
    public String toString() {
        return "Material{" +
                "id=" + id +
                ", fechaCompra=" + fechaCompra +
                ", restringido=" + restringido +
                ", tema=" + tema +
                '}';
    }
}
