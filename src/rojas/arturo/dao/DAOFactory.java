package rojas.arturo.dao;

import rojas.arturo.bl.entities.material.IMaterialDAO;
import rojas.arturo.bl.entities.material.audiovisual.audio.IAudioDAO;
import rojas.arturo.bl.entities.material.audiovisual.texto.ITextoDAO;
import rojas.arturo.bl.entities.material.audiovisual.video.IVideoDAO;
import rojas.arturo.bl.entities.material.otro.IOtroDAO;
import rojas.arturo.bl.entities.usuario.IUsuarioDAO;

public abstract class DAOFactory {
    public static int SQLSERVER=1;
    public static int TEXT_FILE = 2;

    public static DAOFactory getDaoFactory(int whichFactory){
        switch (whichFactory){
            case 1:
                return new SqlServerDaoFactory();
            case 2:
                return new TextFileDaoFactory();
            default:
                return null;
        }
    }
    public abstract IMaterialDAO getMaterialDAO();
    public abstract IUsuarioDAO getUsuarioDAO();
}
