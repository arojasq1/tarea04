package rojas.arturo.dao;

import rojas.arturo.bl.entities.material.IMaterialDAO;
import rojas.arturo.bl.entities.material.MaterialTextoDAO;
import rojas.arturo.bl.entities.material.audiovisual.audio.IAudioDAO;
import rojas.arturo.bl.entities.material.audiovisual.texto.ITextoDAO;
import rojas.arturo.bl.entities.material.audiovisual.video.IVideoDAO;
import rojas.arturo.bl.entities.material.otro.IOtroDAO;
import rojas.arturo.bl.entities.usuario.IUsuarioDAO;
import rojas.arturo.bl.entities.usuario.UsuarioTextoDAO;

public class TextFileDaoFactory extends DAOFactory {

    @Override
    public IMaterialDAO getMaterialDAO() {
        return new MaterialTextoDAO();
    }

    @Override
    public IUsuarioDAO getUsuarioDAO() {
        return new UsuarioTextoDAO();
    }
}