package rojas.arturo.dao;

import rojas.arturo.bl.entities.material.IMaterialDAO;
import rojas.arturo.bl.entities.material.MaterialDAO;
import rojas.arturo.bl.entities.material.audiovisual.audio.AudioDAO;
import rojas.arturo.bl.entities.material.audiovisual.audio.IAudioDAO;
import rojas.arturo.bl.entities.material.audiovisual.texto.ITextoDAO;
import rojas.arturo.bl.entities.material.audiovisual.texto.TextoDAO;
import rojas.arturo.bl.entities.material.audiovisual.video.IVideoDAO;
import rojas.arturo.bl.entities.material.audiovisual.video.VideoDAO;
import rojas.arturo.bl.entities.material.otro.IOtroDAO;
import rojas.arturo.bl.entities.material.otro.OtroDAO;
import rojas.arturo.bl.entities.usuario.IUsuarioDAO;
import rojas.arturo.bl.entities.usuario.UsuarioDAO;

public class SqlServerDaoFactory extends DAOFactory{

    @Override
    public IMaterialDAO getMaterialDAO() {
        return new MaterialDAO();
    }

    @Override
    public IUsuarioDAO getUsuarioDAO() {
        return new UsuarioDAO();
    }
}